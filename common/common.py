class Common(object):
    APP_NAME = 'NavalBattle'
    BYTES = 655350
    ENCODE = 'UTF-8'
    HOST = 'localhost'
    PORT = 3000
    DATA_FOLDER_NAME = 'data'
    TIMEOUT = 3
