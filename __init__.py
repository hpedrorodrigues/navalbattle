import sys

from starter import Starter

if len(sys.argv) > 1:
    argument = sys.argv[1]

    if argument == 'client':
        Starter.client()
    elif argument == 'server':
        Starter.server()
