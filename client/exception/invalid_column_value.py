class InvalidColumnValueException(Exception):
    def __init__(self, column_value):
        super(InvalidColumnValueException, self).__init__('Invalid column value \"%s\"' % str(column_value))
