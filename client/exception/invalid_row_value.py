class InvalidRowValueException(Exception):
    def __init__(self, row_value):
        super(InvalidRowValueException, self).__init__('Invalid row value \"%s\"' % str(row_value))
