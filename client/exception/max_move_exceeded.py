class MaxMoveValueExceededException(Exception):
    def __init__(self, move):
        super(MaxMoveValueExceededException, self).__init__('Max move value exceeded \"%s\"' % str(move))
