from common.common import Common


class Printer(object):
    @staticmethod
    def print_connecting_error():
        print('An error ocurred trying to connect with server. Please, try again.')

    @staticmethod
    def print_choice():
        print('---------------Welcome to %s' % Common.APP_NAME)
        print('1 - Play')
        print('2 - Display board')
        print('3 - Display real board')
        print('4 - Restart')
        print('5 - Clear console')
        print('6 - Exit', end='\n\n')

    @staticmethod
    def print_remaining_moves(remaining):
        print("Your remaining moves: %s" % str(remaining), end='\n\n')

    @staticmethod
    def print_invalid_choice():
        print("Invalid choice. Please, try again.", end='\n\n')

    @staticmethod
    def print_exception(exception):
        print(str(exception))

    @staticmethod
    def print_message(message):
        print(message)
