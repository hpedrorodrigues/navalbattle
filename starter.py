import logging

from client.client import Client
from server.server import Server


class Starter(object):
    @staticmethod
    def __internal_start():
        logging.basicConfig(level=logging.DEBUG)

    @staticmethod
    def client():
        Starter.__internal_start()
        client = Client()

        try:
            client.start()
        except (KeyboardInterrupt, SystemExit):
            pass
        except:
            logging.exception("Unexpected exception")

        logging.info("Client done")

    @staticmethod
    def server():
        Starter.__internal_start()
        server = Server()

        try:
            server.start()
        except (KeyboardInterrupt, SystemExit):
            pass
        except:
            logging.exception("Unexpected exception")
        finally:
            server.shutdown()

        logging.info("Server done")
