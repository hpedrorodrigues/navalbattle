# Naval Battle

A simple game of Naval Battle.

A school work.

**Versions of this project**:

- Single Client + Socket Manipulation + UDP: [here][sc]
- Single Client + RPC (RPyC) + TCP: [here][sc-rpc-pyc]
- Single Client + RPC (JSON-RPC) + TCP: [here][sc-rpc-jsonrpc]
- Single Client + Multi Processing + Queue Server (ZMQ) + TCP: [here][mc-zmq]
- Multi Client + Socket Manipulation + Multi Processing + UDP: [here][mc]

## Running

To run this project just type `python3 __init__.py` in your shell.

## License

This project is released under the MIT license. See 
[LICENSE](./LICENSE) for details.

[sc]: https://github.com/hpedrorodrigues/NavalBattle
[sc-rpc-pyc]: https://github.com/hpedrorodrigues/NavalBattle/tree/rpc-pyc
[sc-rpc-jsonrpc]: https://github.com/hpedrorodrigues/NavalBattle/tree/jsonrpc
[mc]: https://github.com/hpedrorodrigues/NavalBattle/tree/multi_client
[mc-zmq]: https://github.com/hpedrorodrigues/NavalBattle/tree/queue_server